# FastAi + MLflow - AiLab Studies

Nesse repositório se encontra um código em Python com a aplicação de um segmentador de textos com as bibliotecas **FastAi** e **MLflow**.

A base de dados usada no estudo é a **Tobacco800** transformada no formato de texto.

Para rodar, instale as bibliotecas em ˋrequirements.txtˋ e rode o código ˋtext_classification.pyˋ.

- Com isso, uma nova run será criada pelo MLflow, onde os artefatos, parâmetros e métricas já são automaticamente salvos pela biblioteca.

*Caso deseje ver todas as runs e seus componentes, rode ˋmlflow uiˋ no terminal*